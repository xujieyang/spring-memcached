package com.xjy.spring.memcached.spy;

import java.util.concurrent.ExecutionException;

import net.spy.memcached.MemcachedClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xjy.spring.memcached.core.MemcachedConnection;

public class SPYMemcachedConnection implements MemcachedConnection {
	private static final Logger LOGGER = LoggerFactory.getLogger(SPYMemcachedConnection.class);
	private MemcachedClient connection;
	public SPYMemcachedConnection() {
	}
	
	public SPYMemcachedConnection(MemcachedClient connection) {
		super();
		this.connection = connection;
	}

	@Override
	public <T> boolean set(String key, int exp, T value) {
		try {
			return connection.set(key, exp, value).get();
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Use SPYMemcachedConnection, operation is set, key is " + key + ", value is " + value + ",exp is" + exp, e);
		}
		return false;
	}
	
	@Override
	public <T> T get(String key) {
		return (T) connection.get(key);
	}
	
	@Override
	public boolean delete(String key) {
		try {
			return connection.delete(key).get();
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Use SPYMemcachedConnection, operation is delete, key is " + key, e);
		}
		return false;
	}

	@Override
	public MemcachedClient getNativeConnection() {
		return connection;
	}

}
