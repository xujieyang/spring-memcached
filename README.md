#spring-memcached使用

### 1 在spring中定义
```xml
<bean id="spyMemcachedConnectionFactory" class="com.xjy.spring.memcached.spy.SPYMemcachedConnectionFactory">
		<!-- 以','分隔 -->
		<property name="servers" value="127.0.0.1:11211" />
		<property name="protocol">
			<!-- 注入枚举类型 -->
			<value type="net.spy.memcached.ConnectionFactoryBuilder.Protocol">TEXT</value>
		</property>
		<property name="transcoder">
			<bean class="net.spy.memcached.transcoders.SerializingTranscoder" />
		</property>
		<property name="hashAlg">
			<value type="net.spy.memcached.DefaultHashAlgorithm">KETAMA_HASH</value>
		</property>
		<property name="locatorType">
			<value type="net.spy.memcached.ConnectionFactoryBuilder.Locator">ARRAY_MOD</value>
		</property>
	</bean>
	<bean id="xmemcachedConnectionFactory" class="com.xjy.spring.memcached.xm.XMemcachedConnectionFactory">
		<!-- 以','分隔 -->
		<property name="servers" value="127.0.0.1:11211" />
		<property name="failureMode" value="true"/>
		<property name="name" value="materBuilder"/>
	</bean>
	<bean id="spyMemcachedTemplate" class="com.xjy.spring.memcached.core.MemcachedTemplate">
		<property name="connectionFactory" ref="spyMemcachedConnectionFactory"/>
	</bean>
	<bean id="xmemcachedTemplate" class="com.xjy.spring.memcached.core.MemcachedTemplate">
		<property name="connectionFactory" ref="xmemcachedConnectionFactory"/>
	</bean>
```
### 2. 测试使用
```java
package com.xjy.spring.memcached;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xjy.spring.memcached.core.MemcachedTemplate;


public class TestSpringDataMemcached {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("classpath:spring.xml");
		MemcachedTemplate memcachedTemplate = ac.getBean("xmemcachedTemplate", MemcachedTemplate.class);
		memcachedTemplate.opsForValue().set("TestSpringDataMemcached_111", 0, "TestSpringDataMemcached_111");
		System.out.println(memcachedTemplate.opsForValue().get("TestSpringDataMemcached_111"));
		ac.destroy();
	}
}

```